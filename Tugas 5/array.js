var pembatas = "=============================="

console.log("\n" + pembatas + "\n SOAL NO 1 - RANGE \n" + pembatas)
function range(startNum, finishNum){
    var num = []
    if (startNum==null || finishNum==null){
        return num = -1
    } else if (startNum>finishNum){ 
        for (x = finishNum; x <=startNum; x++){
            num.push(x)
        }
        num.sort(function (finishNum, startNum) { return startNum - finishNum } )
    } else {
        for (x = startNum; x <=finishNum; x++){
            num.push(x)
        }
    }
    return num;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 




console.log("\n" + pembatas + "\n SOAL NO 2 - RANGE WITH STEP\n" + pembatas)
function rangeWithStep(startNum, finishNum, step) {
    var num = []
    var a = startNum
    if (startNum>finishNum){
            while (a>=finishNum) {
                    num.push(a)
                    a-=step
                }
                num.sort(function (finishNum, startNum) { return startNum - finishNum } )
            } else {
        while (a<=finishNum) {
                num.push(a)
                a+=step
        }
    }
    return num
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 



console.log("\n" + pembatas + "\n SOAL NO 3 - SUM OF RANGE\n" + pembatas)
function sum(startNum, finishNum, step){
    var num = []
    var total = 0
    var b
    var a = startNum
    
    // cek isi masing-masing parameter
    if (!step){
        step = 1
        if (!finishNum){
            finishNum=startNum
        }
        if (!startNum){
            startNum==0
        }
    }
    
    // mengisi array
    if (startNum>finishNum){
        while (a>=finishNum) {
            num.push(a)
            a-=step
        }
        num.sort(function (finishNum, startNum) { return startNum - finishNum } )
    } else while (a<=finishNum) {
        num.push(a)
        a+=step
    }

    // menjumlah array
    for (b=0;b<num.length;b++){
            total+=num[b];
        }
        return total
    }
console.log(sum(1,10,1)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 




console.log("\n" + pembatas + "\n SOAL NO 4 - ARRAY MULTIDIMENSI\n" + pembatas)
var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

for (a=0;a<=3;a++){
        console.log ("Nomor ID : " + input[a][0])
        console.log ("Nama Lengkap : " + input[a][1])
        console.log ("TTL : " + input[a][2] + " " + input [a][3])
        console.log ("Hobi : " + input[a][4])
        console.log ("\n")
    }




console.log("\n" + pembatas + "\n SOAL NO 5 - BALIK KATA \n" + pembatas)
function balikKata(kata){
    var x = kata.length-1
    var gabung = kata.slice(kata.length-1,kata.length)
    var arr
    while (x>0) {
        arr = kata.slice(x-1,x)
        x--
        gabung+=arr
    }
    return gabung
}    
    
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 




console.log("\n" + pembatas + "\n SOAL NO 5 - BALIK KATA \n" + pembatas)

var input = "0001 , Roman Alamsyah , Bandar Lampung, 21/05/1989, Membaca"
function dataHandling2(input){
    var biodata = input.split(",")
    console.log(biodata)
    var tanggal = biodata[3].split("/")
    var bulan = tanggal[1]
    switch (bulan) {
        case '01': bulan = 'Januari'; break; 
        case '02': bulan = 'Februari'; break; 
        case '03': bulan = 'Maret'; break; 
        case '04': bulan = 'April'; break; 
        case '05': bulan = 'Mei'; break; 
        case '06': bulan = 'Juni'; break; 
        case '07': bulan = 'Juli'; break; 
        case '08': bulan = 'Agustus'; break; 
        case '09': bulan = 'September'; break; 
        case '10': bulan = 'Oktober'; break; 
        case '11': bulan = 'November'; break; 
        case '12': bulan = 'Desember'; break;  
        default : bulan = 'Tanggal salah' ;
    } 
    console.log(bulan)
    var newTanggal = tanggal.sort(function (value1, value2) { return value2 - value1 } )
    console.log(newTanggal)
    var tanggal2 = biodata[3].split("/")
    console.log(tanggal2.join("-"))
    console.log(biodata[1])
}

dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 