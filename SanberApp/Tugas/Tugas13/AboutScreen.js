import React from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const AboutScreen = () => {
   return (
      <ScrollView>
         <View style={styles.container}>
            <Text style={styles.judul}>Tentang Saya</Text>
            <Image
               source={require('./images/profil.png')}
               style={styles.profil}
            />
            <Text style={styles.nama}>Anton Hermawan</Text>
            <Text style={styles.identitas}>React Native Developer</Text>
            <View style={styles.portofolio}>
               <Text style={styles.textJudul}>Portofolio</Text>
               <View style={styles.formPort}>
                  <View style={styles.itemPort}>
                     <FontAwesome name="gitlab" size={40} color={'#3EC6FF'} />
                     <Text style={styles.text}>@4ntonsosmed</Text>
                  </View>
                  <View style={styles.itemPort}>
                     <FontAwesome name="github" size={40} color={'#3EC6FF'} />
                     <Text style={styles.text}>@4ntonsosmed</Text>
                  </View>
               </View>
            </View>
            <View style={styles.hubungi}>
               <Text style={styles.textJudul}>Hubungi Saya</Text>
               <View style={styles.formHubungi}>
                  <FontAwesome
                     name="facebook-square"
                     size={40}
                     color={'#3EC6FF'}
                     style={styles.icon}
                  />
                  <Text style={styles.text}>@promowebz</Text>
                  <FontAwesome
                     name="instagram"
                     size={40}
                     color={'#3EC6FF'}
                     style={styles.icon}
                  />
                  <Text style={styles.text}>@anton4gb</Text>
                  <FontAwesome
                     name="twitter"
                     size={40}
                     color={'#3EC6FF'}
                     style={styles.icon}
                  />
                  <Text style={styles.text}>@anton</Text>
               </View>
            </View>
         </View>
      </ScrollView>
   );
};

export default AboutScreen;

const styles = StyleSheet.create({
   container: {
      flex: 1,
      paddingTop: 64,
      alignItems: 'center',
   },
   judul: {
      fontSize: 36,
      marginBottom: 12,
      fontWeight: 'bold',
      color: '#003366',
   },
   profil: {
      width: 200,
      height: 200,
      borderRadius: 100,
   },
   nama: {
      fontSize: 20,
      color: '#003366',
      marginTop: 24,
      fontWeight: 'bold',
   },
   identitas: {
      color: '#3EC6FF',
   },
   icon: {
      textAlign: 'center',
   },
   portofolio: {
      backgroundColor: '#EFEFEF',
      width: '95%',
      height: 140,
      marginTop: 16,
      borderRadius: 10,
      padding: 10,
      color: '#003366',
   },
   formPort: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      borderTopWidth: 2,
      borderTopColor: '#003366',
      padding: 20,
   },
   textJudul: {
      color: '#003366',
      fontSize: 18,
   },
   text: {
      color: '#003366',
      textAlign: 'center',
      marginBottom: 10,
   },
   itemPort: {
      alignItems: 'center',
   },
   hubungi: {
      backgroundColor: '#EFEFEF',
      width: '95%',
      height: 280,
      marginVertical: 16,
      borderRadius: 10,
      padding: 10,
      color: '#003366',
   },
   formHubungi: {
      borderTopWidth: 2,
      borderTopColor: '#003366',
      padding: 20,
   },
});
