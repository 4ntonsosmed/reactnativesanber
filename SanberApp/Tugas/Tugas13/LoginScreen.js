import React from 'react';
import {
   Image,
   ScrollView,
   StyleSheet,
   Text,
   TextInput,
   TouchableOpacity,
   View,
} from 'react-native';

const LoginScreen = () => {
   return (
      <ScrollView>
         <View style={styles.container}>
            <Image
               source={require('./images/logo.png')}
               style={{ width: 270, height: 80 }}
            />
            <Text style={styles.judul}>Login</Text>
            <View>
               <Text style={styles.input}>Username / Email</Text>
               <TextInput style={styles.formInput} />
            </View>
            <View>
               <Text style={styles.input}>Password</Text>
               <TextInput style={styles.formInput} secureTextEntry />
            </View>
            <View style={{ marginTop: 30, alignItems: 'center' }}>
               <TouchableOpacity style={styles.textButton2}>
                  <Text style={{ color: 'white' }}>Masuk</Text>
               </TouchableOpacity>
               <Text style={{ marginVertical: 16, color: '#003366' }}>
                  atau
               </Text>
               <TouchableOpacity style={styles.textButton1}>
                  <Text style={{ color: 'white' }}>Daftar</Text>
               </TouchableOpacity>
            </View>
         </View>
      </ScrollView>
   );
};

export default LoginScreen;

const styles = StyleSheet.create({
   container: {
      flex: 1,
      paddingTop: 60,
      paddingHorizontal: 40,
   },
   judul: {
      marginVertical: 40,
      fontSize: 20,
      color: '#003366',
      textAlign: 'center',
   },
   input: {
      color: '#003366',
   },
   formInput: {
      borderWidth: 1,
      borderColor: '#003366',
      height: 48,
      marginBottom: 16,
   },
   textButton1: {
      width: 140,
      height: 40,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#003366',
   },
   textButton2: {
      width: 140,
      height: 40,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#3EC6FF',
   },
});
