import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import Component from './Latihan/Component/Component';
// import Props from './Latihan/Component/Props';
// import State from './Latihan/Component/State';
// import Styling from './Latihan/Styling/index.js';
// import YoutubeUI from './Tugas/Tugas12/App.js';
// import Login from './Tugas/Tugas13/LoginScreen.js';
// import AboutSreen from './Tugas/Tugas13/AboutScreen.js';
import Tugas14 from './Tugas/Tugas14/App';

export default function App() {
   return (
      // <Component />
      // <Login />
      // <Props />
      // <State />
      // <AboutSreen />
      // <YoutubeUI />
      // <View style={styles.container}>
      //   <Text>Aplikasi yang dijalankan dengan Expo</Text>
      //   <StatusBar style="auto" />
      // </View>
      <Tugas14 />
   );
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
   },
});
