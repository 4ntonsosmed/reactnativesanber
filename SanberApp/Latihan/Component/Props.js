import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

const Story = (props) => {
   return (
      <View>
         <Image
            source={require('./images/logo.png')}
            style={{ width: 98, height: 20 }}
         />
         <Text>{props.judul}</Text>
      </View>
   );
};

const Props = () => {
   return (
      <View>
         <Text>Komponen Dinamis dengan Props</Text>
         <Story judul="Anton Channel" />
         <Story judul="Agung Channel" />
         <Story judul="Putri Channel" />
      </View>
   );
};

export default Props;

const styles = StyleSheet.create({});
