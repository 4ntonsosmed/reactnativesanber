import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Tes = () => {
   return (
      <View style={styles.container}>
         <View style={styles.navBar}>
            <Image
               source={require('./images/logo.png')}
               style={{ width: 98, height: 22 }}
            />
            <View style={styles.rightNav}>
               <TouchableOpacity style={styles.navItem}>
                  <Icon name="search" size={25} />
               </TouchableOpacity>
               <TouchableOpacity style={styles.navItem}>
                  <Icon name="account-circle" size={25} />
               </TouchableOpacity>
            </View>
         </View>

         <View style={styles.body}></View>

         <View style={styles.tabBar}>
            <TouchableOpacity style={styles.tabItem}>
               <Icon name="home" size={25} />
               <Text style={styles.tabTitle}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem}>
               <Icon name="whatshot" size={25} />
               <Text style={styles.tabTitle}>Trending</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem}>
               <Icon name="subscriptions" size={25} />
               <Text style={styles.tabTitle}>Subscriptions</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.tabItem}>
               <Icon name="folder" size={25} />
               <Text style={styles.tabTitle}>Library</Text>
            </TouchableOpacity>
         </View>
      </View>
   );
};

const styles = StyleSheet.create({
   container: {
      flex: 1,
   },

   navBar: {
      backgroundColor: 'white',
      height: 55,
      elevation: 3,
      flexDirection: 'row',
      paddingHorizontal: 15,
      alignItems: 'center',
      justifyContent: 'space-between',
   },

   rightNav: {
      flexDirection: 'row',
   },

   navItem: {
      marginLeft: 15,
   },

   body: {},

   tabBar: {
      height: 60,
      backgroundColor: 'white',
      borderTopWidth: 0.5,
      borderColor: '#E5E5E5',
      flexDirection: 'row',
      justifyContent: 'space-around',
   },

   tabItem: {
      justifyContent: 'center',
      alignItems: 'center',
   },

   tabTitle: {
      fontSize: 11,
      paddingTop: 4,
      color: '#3c3c3c',
   },
});
export default Tes;
