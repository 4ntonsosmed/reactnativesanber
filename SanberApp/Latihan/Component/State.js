import React, { Component, useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

const Counter = () => {
   const [number, setNumber] = useState(0);
   return (
      <View>
         <Text>{number}</Text>
         {/* <Button title="Tambah" onPress={() => alert('Hello button')} /> */}
         <Button title="Tambah" onPress={() => setNumber(number + 1)} />
      </View>
   );
};

class CounterClass extends Component {
   state = {
      number: 0,
   };
   render() {
      return (
         <View>
            <Text>{this.state.number}</Text>
            {/* <Button title="Tambah" onPress={() => alert('Hello button')} /> */}
            <Button
               title="Tambah"
               onPress={() => this.setState({ number: this.state.number + 1 })}
            />
         </View>
      );
   }
}

const State = () => {
   return (
      <View style={styles.container}>
         <Text style={styles.judul}>Component Dinamis dengan State</Text>
         <Text>State dengan Fungsional Component (Hooks)</Text>
         <Counter />
         <Text>State dengan Class Component</Text>
         <CounterClass />
      </View>
   );
};

export default State;

const styles = StyleSheet.create({
   container: {
      margin: 30,
   },
   judul: {
      textAlign: 'center',
      fontWeight: 'bold',
      marginBottom: 20,
   },
});
