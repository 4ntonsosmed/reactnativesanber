var pembatas = "==========================================="

console.log("\n" + pembatas + "\n 1. Mengubah fungsi menjadi fungsi arrow \n" + pembatas)
const golden = () => {console.log(`this is golden!!`)}
golden()




console.log("\n" + pembatas + "\n 2. Sederhanakan menjadi Object literal di ES6 \n" + pembatas)
const newFunction = (firstName, lastName) => {
  console.log(firstName + " " + lastName)
}

newFunction("William", "Imoh")




console.log("\n" + pembatas + "\n 3. Destructuring \n" + pembatas)
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
//   dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;
// //   Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
const {firstName, lastName, destination, occupation} = newObject;

//   // Driver code
console.log(firstName, lastName, destination, occupation)




console.log("\n" + pembatas + "\n 4. Array Spreading \n" + pembatas)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
const combinedArray = [...west, ...east]
//Driver Code
console.log(combined)
console.log(combinedArray)




console.log("\n" + pembatas + "\n 5. Template Literals \n" + pembatas)
const planet = "earth"
const view = "glass"
var before = `Lorem  ${view}  dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 