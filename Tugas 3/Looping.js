var pembatas = "===================================="

console.log(pembatas + "\n TUGAS 1 LOOPING WHILE \n" + pembatas)
var tugas1 = 1;
console.log("LOOPING PERTAMA")
while(tugas1 <= 10) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
    console.log(tugas1*2 + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
    tugas1++; // Mengubah nilai flag dengan menambahkan 1
}

var tugas2 = 10;
console.log("LOOPING KEDUA")
while(tugas2 >= 1) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
    console.log(tugas2*2 + ' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
    tugas2--; // Mengubah nilai flag dengan menambahkan 1
}


console.log("\n" + pembatas + "\n TUGAS 2 LOOPING MENGGUNAKAN FOR \n" + pembatas)
for(var x = 1; x <= 20; x++) {
    if (x%2 != 0) {
        if (x%3 == 0) {
            console.log(x + " - I Love Coding")
        } else {
            console.log(x + " - Santai")
        }
    } else {
        console.log(x + " - Berkualitas")
    }
  }




console.log("\n" + pembatas + "\n TUGAS 3 MEMBUAT PERSEGI PANJANG \n" + pembatas)
var persegi = '';
for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 8; j++) {
        persegi += '# ';
    }
    persegi += '\n';
}
console.log(persegi);




console.log("\n" + pembatas + "\n TUGAS 4 MEMBUAT TANGGA \n" + pembatas)
var tangga = '';
for (var a = 0; a < 7; a++) {
    for (var b = 0; b <= a; b++) {
        tangga += '# ';
    }
    tangga += '\n';
}
console.log(tangga);




console.log("\n" + pembatas + "\n TUGAS 5 MEMBUAT PAPAN CATUR \n" + pembatas)
var catur = '';
for (var c = 0; c < 8; c++) {
    for (var d = 0; d < 4; d++) {
        if (c%2==0) {
            catur += ' #';
        } else {
            catur += '# ';
        }
    }
    catur += '\n';
}
console.log(catur);
