var pembatas = "=============================="

console.log("\n" + pembatas + "\n 1. Animal Class (Release 0) \n" + pembatas)
class Animal {
    // Code class di sini
    constructor (name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false




console.log("\n" + pembatas + "\n 1. Animal Class (Release 1) \n" + pembatas)

class Ape extends Animal {
    constructor(name){
        super(name);
        this.legs = 2
    }
    yell() {
        console.log(this.name)
        console.log(this.legs)
        console.log(this.cold_blooded)
        console.log("Auooo");
    } 
}

class Frog extends Animal {
    jump() {
      console.log("Hop hop");  
    } 
}
 
// Code class Ape dan class Frog di sini
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("kodok")
kodok.jump() // "hop hop" 