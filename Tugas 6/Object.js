
var pembatas = "=============================="

console.log("\n" + pembatas + "\n SOAL NO 1 - ARRAY TO OBJECT \n" + pembatas)

function arrayToObject(arr) {
    for (x=0;x<=arr.length-1;x++){
        console.log(x+1 + ". " + arr[x][0] + " " + arr[x][1] + ":")
        var now = new Date()
        var thisYear = now.getFullYear()
        var personObj = {
            firstName : arr[x][0],
            lastName: arr[x][1],
            gender: arr[x][2],
            age: thisYear - arr[x][3]
        }
        if (arr[x][3] == null || arr[x][3]>thisYear){
            personObj.age = "Invalid Birth Year"
        }
        console.log(personObj)
        console.log("\n")
    } 
}
    
// var people = [ "Bruce", "Banner", "male", 1975]
// console.log(people)
// arrayToObject(people) 
// Driver Code 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/