// Tugas Nomor 1
// Menampilkan output : JavaScript is awesome and I love it! 
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
var gabung = word + " " + second + " " + third +  " "  + fourth +  " "  + fifth +  " "  + sixth +  " "  + seventh;
var pembatas = '=============================================';

console.log("\n" + pembatas + "\n")
console.log("Tugas Nomor 1 \n")
console.log(gabung)
console.log("\n" + pembatas + "\n")

// Tugas Nomor 2
var sentence = "I am going to be React Native Developer"; 
// I   am   going      to      be        React          Native           Developer
// 0 1 23 4 56789 10  1112 13 1415 16 1718192021 22 232425262728 29 303132333435363738

var FirstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3] ; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];  
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log("Tugas Nomor 2 \n")
console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)
console.log("\n" + pembatas + "\n")

// Tugas Nomor 3
var sentence2 = 'wow JavaScript is so cool';
//  wow JavaScript   is       so       cool
//  0,3 4,14       15,17    18,20     21,25 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); // do your own! 
var thirdWord2 = sentence2.substring(15, 17); // do your own! 
var fourthWord2 = sentence2.substring(18, 20); // do your own! 
var fifthWord2 = sentence2.substring(21, 25); // do your own! 

console.log("Tugas Nomor 3 \n")
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
console.log("\n" + pembatas + "\n")

// Tugas Nomor 4
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); // do your own! 
var thirdWord3 = sentence3.substring(15, 17); // do your own! 
var fourthWord3 = sentence3.substring(18, 20); // do your own! 
var fifthWord3 = sentence3.substring(21, 25); // do your own! 

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = secondWord3.length  
var thirdWordLength = thirdWord3.length  
var fourthWordLength = fourthWord3.length  
var fifthWordLength = fifthWord3.length 

// lanjutkan buat variable lagi di bawah ini 
console.log("Tugas Nomor 4 \n")
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 
console.log("\n" + pembatas + "\n")