// // Tugas Membuat Conditional If - Else
var pembatas = '=============================================';
var nama = "Anton"
var peran = "Guard"

console.log(pembatas + "\n Tugas Conditional dengan If-Else \n" + pembatas)

if (nama === ""){
    console.log("Nama harus diisi");
} else if (peran === ""){
    console.log("Halo " + nama + " ! Pilih peranmu untuk memulai game !")
} else if (peran === "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (peran === "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo Guard " + nama + ", amu akan membantu melindungi temanmu dari serangan werewolf")
} else if (peran === "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    console.log("Peran yang kamu pilih tidak ada! Silahkan pilih kembali peran yang ada (Penyihir / Guard / Werewolf)")
}




// Tugas Membuat Conditional Switch Case
console.log("\n" + pembatas + "\n Tugas Conditional dengan Switch Case \n" + pembatas)
var tanggal = 30; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 4; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2021; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var peringatan = 'Tanggal tidak tersedia (terlalu besar) di bulan ini ';

// Pengaturan tanggal terakhir setiap bulan
if (bulan > 1 && bulan < 13){
    if (tanggal > 0 && tanggal < 32){
        if (tanggal == 31){
            if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12){
                bulan = bulan
            } else {
                console.log(peringatan + "\n")
                return;
            }
        } else if(tanggal == 30){
            if (bulan != 2){
                bulan = bulan
            } else {
                console.log(peringatan + "\n")
                return;
            }
        } else if (tanggal == 29){
                if (bulan == 2 && tahun % 4 != 0){
                    console.log(peringatan + "\n")
                    return;
                } else {
                    bulan = bulan
                }
        }
    } else {
        console.log("Masukkan tanggal dari 1 - 31")
    }
} else {
    console.log("Masukkan bulan dari 1 - 12")
}

// Penerjemahan bulan dari angka menjadi bulan kalender
switch (bulan) {
    case 1: bulan = 'Januari'; break; 
    case 2: bulan = 'Februari'; break; 
    case 3: bulan = 'Maret'; break; 
    case 4: bulan = 'April'; break; 
    case 5: bulan = 'Mei'; break; 
    case 6: bulan = 'Juni'; break; 
    case 7: bulan = 'Juli'; break; 
    case 8: bulan = 'Agustus'; break; 
    case 9: bulan = 'September'; break; 
    case 10: bulan = 'Oktober'; break; 
    case 11: bulan = 'November'; break; 
    case 12: bulan = 'Desember'; break;  
}

console.log(tanggal + " " + bulan + " " + tahun + "\n")
